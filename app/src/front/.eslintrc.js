module.exports = {
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'standard'
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: [
    'svelte3'
  ],
  // Does not work yet
  // overrides: [
  //   {
  //     files: ['*.svelte'],
  //     processor: 'svelte3/svelte3'
  //   }
  // ],
  rules: {
    "no-extra-semi": "off",
    "semi": ["error", "always"],
    "comma-dangle": ["error", "always-multiline"],
    "camelcase": "off",
    "new-cap": ["error", {"newIsCapExceptionPattern": "^component"}],
  }
}
