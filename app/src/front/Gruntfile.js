const sass = require('node-sass');

module.exports = function(grunt) {

  grunt.initConfig({
    sass: {
      options: {
        implementation: sass,
        sourceMap: false
      },
      dist: {
        files: {
          '/app/www/assets/css/main.css': 'sass/main.scss'
        }
      }
    }
  });

  //require('load-grunt-tasks')(grunt);
  grunt.loadNpmTasks('grunt-sass');
  grunt.registerTask('default', ['sass']);
};
