// rollup.config.js

import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import { nodeResolve } from '@rollup/plugin-node-resolve';

export default {
  input: 'js/main.js',
  output: {
    file: '/app/www/assets/js/main.js',
    format: 'cjs'
  },
  plugins: [
    commonjs(),
    nodeResolve(),
    svelte({
      emitCss: false,
      compilerOptions: {
        customElement: true
      }
    })
  ]
};
