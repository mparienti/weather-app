import component from './current.svelte';
import { getIconPath } from './utils.js';

let cc = {};

const current = {
  init () {
    cc = new component({
      target: document.getElementById('current'),
    },
    );
  },

  translateData (data) {
    data.current_icon = getIconPath(data.weather[0].icon, data.weather[0].id);
    // do some date calculation stuff
    return data;
  },

  updateComponent (data = {}) {
    data = current.translateData(data);
    cc.current = data;
  },

};

export default current;
