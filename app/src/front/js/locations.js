import current from './current.js';
import forecast from './forecast.js';
import { debounce } from './utils.js';

const cache = {};
const datalist_all = {};
let timeout = undefined;

const inputChange = debounce(function (event) {
  const input = event.target;
  const value = input.value;

  for (const property in datalist_all) {
    if (value === datalist_all[property]) {
      locations.setLocation(property);
      break;
    }
  }
});

const inputKey = debounce(function (event) {
  const input = event.target;
  const value = input.value;

  if (value.length < 3) {
    return;
  }

  if (event.which === 13 || event.keyCode === 13) {
    for (const property in datalist_all) {
      if (value === datalist_all[property]) {
        locations.setLocation(property);
        // this does not work :-(
        event.preventDefault();
        break;
      }
    }
  }

  locations.getLocations(input.value).then(values => {
    if (values !== undefined && values.length > 0) {
      locations.updateList(values);
    } else {
      locations.removeDatalist();
    }
  });
});

const locations = {
  initEvent () {
    window.onload = this.attachEvent();
  },

  attachEvent () {
    const input = document.getElementById('input-location');
    input.addEventListener('keypress', inputKey);
    input.addEventListener('input', inputChange);
  },

  async getLocations (text) {
    if (cache[text]) {
      return cache[text];
    }
    // console.log(new URL(`/autocomplete?city=${text}`, location));
    const response = await fetch(new URL(`/autocomplete?city=${text}`, location));
    const data = await response.json();
    if (data.length > 0) {
      cache[text] = data;
      return cache[text];
    }
  },

  updateList (values) {
    const datalist_dom = document.createElement('datalist');
    datalist_dom.id = 'locations-list';
    const input_location = document.getElementById('input-location');
    // https://bugzilla.mozilla.org/show_bug.cgi?id=1474137
    input_location.setAttribute('autocomplete', 'off');

    values.forEach(locations => {
      if (!(locations.id in datalist_all)) {
        datalist_all[locations.id] = locations.label;
      }
      const option = document.createElement('option');
      option.id = locations.id;
      option.setAttribute('value', locations.label);
      option.innerHTML = locations.label;
      datalist_dom.appendChild(option);
    });

    const previous_list = document.getElementById('locations-list');

    if (previous_list != null) {
      previous_list.replaceWith(datalist_dom);
    } else {
      input_location.appendChild(datalist_dom);
    }
    //    input_location.setAttribute('autocomplete', 'on');
  },

  removeDatalist () {
    const previous_list = document.getElementById('locations-list');
    if (previous_list != null) {
      previous_list.remove();
    }
  },

  setLocation(location_id) {
    if (location_id === undefined || location_id === '') {
      return;
    }
    this.triggerRequest (location_id);
    if (timeout !== undefined) {
      clearTimeout(timeout);
    }
    timeout = setTimeout(()=>{ locations.setLocation(location_id) }, 900*1000);
  },

  triggerRequest (location_id) {
    fetch(new URL(`/weather?id=${location_id}`, location))
      .then(response => response.json())
      .then(data => {
        if (data) {
          locations.updateComponents(data);
        } else {
          console.log('no data');
        }
      });
  },

  updateComponents (data) {
    current.updateComponent(data.current);
    forecast.updateComponent(data.forecast);
  },
};

export default locations;
