// Returns a function, that, as long as it continues to be invoked, will not
// be triggered. The function will be called after it stops being called for
// N milliseconds. If `immediate` is passed, trigger the function on the
// leading edge, instead of the trailing.
function debounce (func, wait, immediate) {
  let timeout;
  return function () {
    const context = this; const args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

function getIconPath (icon, code, size = '') {
  let path = '/assets/img/icons/';
  if (size === 'small') {
    path += '22/';
  } else if (size === 'medium') {
    path += '48/';
  } else {
    path += '128/';
  }
  // We don't use code yet
  switch (icon) {
    case '01d':
      return path + 'sun.png';
    case '01n':
      return path + 'sun-night.png';
    case '02d':
      return path + 'lightcloud.png';
    case '02n':
      return path + 'lightcloud-night.png';
    case '03d':
      return path + 'partlycloud.png';
    case '03n':
      return path + 'partlycloud-night.png';
    case '04d':
    case '04n':
      return path + 'cloud.png';
    case '09d':
    case '09n': {
      let base = 'lightrainsun';
      if (code in ['302', '312', '313', '314', '501', '503', '504', '521', '522', '531']) {
        base = 'rain';
      } else if (icon === '09d') {
        base += '-night';
      }
      return path + base + '.png';
    }
    case '10d':
    case '10n':
      return path + 'rain.png';
    case '11d':
      return path + 'lightrainthundersun.png';
    case '11n':
      return path + 'lightrainthundersun-night.png';
    case '13d':
      return path + 'snowsun.png';
    case '13n':
      return path + 'snowsun-night.png';
    case '50d':
    case '50n':
      return path + 'fog.png';
  }

  return path + 'nodata.png';
  /*
    if (code in []) { // cloud
    return path + 'cloud.png';
  } else if (code in []) { // lightcloud
    return path + 'lightcloud.png';
  } else if (code in []) { // partlycloud
    return path + 'partlycloud.png';
  } else if (code in []) { // sleetsunthunder
    return path + 'sleetsunthunder.png';
  } else if (code in []) { // snowsunthunder
    return path + 'snowsunthunder.png';
  } else if (code in []) { // lightrain
    return path + 'lightrain.png';
  } else if (code in []) { // rain
    return path + 'rain.png';
  } else if (code in []) { // sleet
    return path + 'sleet.png';
  } else if (code in []) { // snow
    return path + 'snow.png';
  } else if (code in []) { // sun
    return path + 'sun.png';
  } else if (code in []) { // fog
    return path + 'fog.png';
  } else {
    return path + 'nodata.png';
  } */
}

export { debounce, getIconPath };
