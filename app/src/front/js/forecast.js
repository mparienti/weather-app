import component from './forecast.svelte';
import { getIconPath } from './utils.js';

let cc = {};

const forecast = {
  init () {
    cc = new component({
      target: document.getElementById('forecast'),
    },
    );
  },

  translateData (data) {
    const timezone = data.timezone;

    data.days = [];
    let current_day;
    let index = -1;
    let last_dt;
    data.hourly.hourly.push(...data.daily.list);
    data.hourly.hourly.forEach((element) => {
      if (last_dt > element.dt) {
        return;
      }
      last_dt = element.dt;
      const the_date = new Date(last_dt * 1000).toLocaleString('en-UK', { timeZone: timezone });
      const [day, time] = the_date.split(', ');
      if (day !== current_day) {
        current_day = day;
        data.days.push([]);
        index += 1;
      }
      element.start = time.slice(0, 5);
      element.day = day;
      element.icon = getIconPath(element.weather[0].icon, element.weather[0].id, 'small');
      if (element.main !== undefined) {
        for (const property in element.main) {
          element[property] = element.main[property];
        }
      }
      // add from->to?
      data.days[index].push(element);
    });

    // get last start
    // loop on data.daily.list until start > last_start
    // add
    
    return data;
  },

  updateComponent (data = {}) {
    data = forecast.translateData(data);
    cc.forecast = data;
  },

};

export default forecast;
