import locations from './locations.js';
import current from './current.js';
import forecast from './forecast.js';

const main = function () {
  locations.initEvent();
  current.init();
  forecast.init();
};

main();
