<?php

require __DIR__ . '/vendor/autoload.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;

// for /autocomplete
use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Cache\Adapter\Filesystem\FilesystemCachePool;

// for /weather
use Cmfcmf\OpenWeatherMap;
use Http\Factory\Guzzle\RequestFactory;
use Http\Adapter\Guzzle6\Client as GuzzleAdapter;

$app = AppFactory::create();

$app->get(
    '/',
    function (Request $request, Response $response) {
        $response->getBody()->write(file_get_contents(__DIR__ . '/tpl/index.html'));
        return $response;
    }
);

//
$app->get(
    '/weather',
    function (Request $request, Response $response) {
        $place_id = @$request->getQueryParams()['id'];
        if ($place_id === null) {
            $response->getBody()->write('[]');
            return $response->withHeader('Content-type', 'application/json');
        }
        $place = explode('|', $place_id);
        $lat = (float) $place[0];
        $lon = (float) $place[1];

        require_once('config.php');
        try {
            $data = [];

            // Current API return city name, which is nice. That's why there is 2 calls
            $url_current = "https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}" .
                 "&units=metric&appid=" . OPENWEATHER_API_KEY;
            $data['current'] = json_decode(file_get_contents($url_current, false));

            // Get hourly for 3 days
            $url_hourly = "https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lon}" .
                 "&units=metric&exclude=minutely,daily,alerts,current&appid=" . OPENWEATHER_API_KEY;
            $data['forecast']['hourly'] = json_decode(file_get_contents($url_hourly, false));

            // Get 3 hours for 5 days
            $url_forecast = "https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${lon}" .
                 "&units=metric&appid=" . OPENWEATHER_API_KEY;
            $data['forecast']['daily'] = json_decode(file_get_contents($url_forecast, false));

            $response->getBody()->write(json_encode($data));
        } catch (Exception $e) {
            $response->getBody()->write('[]');
        }

        return $response->withHeader('Content-type', 'application/json');
    }
);


$app->get(
    '/autocomplete',
    function (Request $request, Response $response) {
        $filesystemAdapter = new Local('/tmp/cache');
        $filesystem        = new Filesystem($filesystemAdapter);
        $pool = new FilesystemCachePool($filesystem);
        require_once __DIR__.'/utils.php';

        try {
            $baseUrl = 'https://nominatim.openstreetmap.org/search?';
            $city = @$request->getQueryParams()['city'];
            $json = getJson($u="{$baseUrl}q={$city}&format=json", $pool);
            $response_array = [];

            if (is_array($json)) {
                foreach ($json as $geoname) {
                    if (!isset($geoname->lon) || !isset($geoname->lat)) {
                        continue;
                    }
                    $response_array[] = array('id' => $geoname->lat . '|' . $geoname->lon,
                                        'label' => $geoname->display_name,
                    );
                }
            }
            $response->getBody()->write(json_encode($response_array));
            return $response->withHeader('Content-type', 'application/json');
        } catch (Exception $e) {
            $response->getBody()->write(json_encode($e));
            return $response->withHeader('Content-type', 'application/json');
        }
    }
);

$app->run();
