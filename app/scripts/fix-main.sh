#!/usr/bin/env bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

cd ${SCRIPT_DIR}/../www/assets/js

INLINE=$( grep  \<style main.js |grep .forecast_single|cut -d \> -f2 | cut -d \< -f1 )

echo $INLINE

sed -i "s/<style>p/<style>${INLINE}p/" main.js

