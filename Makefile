.PHONY: bash-dev-env build-dev-env web-up build-backend build-frontend

pwd := $(shell pwd)
project_name := $(shell grep ^COMPOSE_PROJECT_NAME $(pwd)/docker/.env | cut -f 2 -d \=)
dc-dev-env = docker-compose -f $(pwd)/docker/docker-compose-dev-env.yml --env-file $(pwd)/docker/.env
dc-web = docker-compose -f $(pwd)/docker/docker-compose-web.yml --env-file $(pwd)/docker/.env

build-dev-env:
	docker rmi $(project_name)_dev || true # delete previous image (if it exists)
	$(dc-dev-env) build

bash-dev-env:
	$(dc-dev-env) run --rm dev bash

web-up:
	$(dc-web) up

build-backend:
	$(dc-dev-env) run --rm dev bash -c "cd /app/src && composer install"

lint-frontend:
	$(dc-dev-env) run --rm dev bash -c "cd /app/src/front && yarn run eslint js/*.js "

lint-backend:
	$(dc-dev-env) run --rm dev bash -c "cd /app/src/php && ../tools/php-cs-fixer fix ."

lint: lint-backend lint-frontend

build-frontend:
	$(dc-dev-env) run --rm dev bash -c "cd /app/src/front && yarn rollup -c && bash /app/scripts/fix-main.sh  && grunt"

update-www:
	rsync -rcv app/src/php/* app/www
	rsync -rcv app/src/vendor app/www
	rsync -rcv app/src/assets/* app/www/assets/

build: build-backend build-frontend update-www
